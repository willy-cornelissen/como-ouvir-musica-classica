﻿1
00:00:04,986 --> 00:00:48,468
Exposição
Tema A
"O destino batendo a sua porta"

2
00:00:50,664 --> 00:00:54,052
As trompas anunciam novo tema

3
00:00:54,152 --> 00:01:33,947
Exposição
Tema B

4
00:01:35,180 --> 00:02:22,448
Exposição
Tema A
Repetição

5
00:02:22,548 --> 00:02:25,759
As trompas anunciam a repetição
do novo tema

6
00:02:25,859 --> 00:03:06,380
Exposição
Tema B
Repetição

7
00:03:07,780 --> 00:03:56,906
Desenvolvimento:
Repete o tema completo

8
00:03:58,166 --> 00:04:09,102
Desenvolvimento:
Reduz o tema para duas notas

9
00:04:11,106 --> 00:04:29,062
Desenvolvimento:
Reduz o tema para uma nota

10
00:04:31,350 --> 00:04:40,463
Termina o Desenvolvimento
Toda a orquestra

11
00:04:40,563 --> 00:04:51,748
Recapitulação?

12
00:04:53,023 --> 00:05:08,265
Ainda não
Primeiro um solo de oboé

13
00:05:09,935 --> 00:05:27,503
Recapitulação
Tema A

14
00:05:27,603 --> 00:05:30,684
Na repetição os fagotes
anunciam a repetição do novo tema

15
00:05:31,723 --> 00:05:57,447
Recapitulação
Tema B

16
00:05:57,547 --> 00:07:39,388
Uma longa coda

