﻿1
00:00:04,480 --> 00:00:30,120
1. Barroco
Antovio Vivaldi, 1711
Concerto em Lá Maior, RV356

2
00:00:35,501 --> 00:01:11,822
2. Romantismo
Richard Strauss, 1915
Eine Alpen Sinfonie

3
00:01:16,108 --> 00:01:44,961
3. Medieval
Guillaume de Machaut, 1365
Missa de Notre Dame (Kyrie)

4
00:01:49,222 --> 00:02:15,385
4. Modernismo
Arnold Schoenberg, 1942
Piano Concerto, Op. 42

5
00:02:20,301 --> 00:02:46,980
5. Romantismo
Richard Wagner, 1868
Mestres Cantores de Nuremberg

6
00:02:51,780 --> 00:03:15,260
6. Barroco
Johann Sebastian Bach, 1721-1725
Concerto de Brandenburgo, No. 5

7
00:03:19,204 --> 00:03:52,401
7. Classicismo / Romantismo
Beethoven, 1804-1808
5.a Sinfonia

8
00:03:56,980 --> 00:04:29,240
8. Classicismo
Mozart, 1786
O casamento de Fígaro

9
00:04:33,581 --> 00:05:00,900
9. Renascimento
Josquin Des Prez, 1475
Ave Maria...Virgo Serena

10
00:05:15,460 --> 00:05:31,302
10. Romantismo
Gustav Holst, 1914
Os planetas, Opus 32, Marte

